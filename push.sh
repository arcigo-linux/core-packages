#!/usr/bin/env bash

# Desc: Push files to gitlab

echo -e "\n==> Adding files to git\n"
git add .

if [[ $1 == "" ]]; then
    echo -e "\n==> Commiting files 'Updated'\n"
    git commit -m "Updated"
else
    echo -e "\n==> Commiting files '$1'\n" 
    git commit -m $1
fi

echo -e "\n==> Pushing files to gitlab\n"
git push
