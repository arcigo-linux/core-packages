#!/usr/bin/env bash

# Desc: This shell script allows to build packages using the PKGBUILD
#
# Copyright (C) 2021-2022 Arcigo Linux <arcigo.linux@gmail.com>
# Everyone is permitted to copy and distribute copies of this file under GNU-GPL3
#

# Dirs
DIR="$(pwd)"
PKGS=(`ls -d */ | cut -f1 -d'/'`)
PKGDIR="$DIR/packages"

# Script Termination
exit_on_signal_SIGINT () {
    { printf "%s\n" "==> Script interrupted !" 2>&1; echo; }
    exit 0
}

exit_on_signal_SIGTERM () {
    { printf "%s\n" "==> Script terminated !" 2>&1; echo; }
    exit 0
}

_green_text(){
	QUERY="$*"
	echo -e "\e[32m${QUERY}\e[0m"
}

_red_text(){
	QUERY="$*"
	echo -e "\e[31m${QUERY}\e[0m"
}

trap exit_on_signal_SIGINT SIGINT
trap exit_on_signal_SIGTERM SIGTERM

# Build packages
build_pkgs () {
	local pkg

	if [[ ! -d "$PKGDIR" ]]; then
		mkdir -p "$PKGDIR"
	fi

	_green_text "\n==> Building Packages\n"
	for pkg in "${PKGS[@]}"; do
		_green_text " -> Building ${pkg}"
		cd ${pkg} && updpkgsums && makepkg -s && mv *.pkg.tar.zst "$PKGDIR"

		if [[ "$pkg" == 'calamares' ]]; then
			rm -rf src pkg *.tar.gz
		elif [[ "$pkg" == 'grub' ]]; then
			rm -rf src pkg gnulib grub grub-extras unifont-*
	    elif [[ "$pkg" == 'pup-git' ]]; then
	            rm -rf pkg/ src/ pup/
		else
			rm -rf src pkg *.tar.gz
		fi

		# Verify
		while true; do
			set -- "$PKGDIR"/${pkg}-*
			if [[ -e "$1" ]]; then
				_green_text "\n-> Package '${pkg}' generated successfully.\n"
				break
			else
				_red_text "\n-> Failed to build '${pkg}', Exiting...\n"
				exit 1;
			fi
		done
		cd "$DIR"
	done

	_green_text "[*] Want to move packages to repository ?\n" && read -s

	REPO_DIR='../arcigo-repo/x86_64'
	if [[ -d "$REPO_DIR" ]]; then
		mv -f "$PKGDIR"/*.pkg.tar.zst "$REPO_DIR" && rm -r "$PKGDIR"
		_green_text "==> Packages moved to Repository.\n[!] Don't forget to update the database.\n"
	fi
}

# Execute
build_pkgs
